import { DatabaseConfigProvider } from './../../providers/database-config/database-config';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConfiguracionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configuracion',
  templateUrl: 'configuracion.html',
})
export class ConfiguracionPage {

  public claveConfiguracion: string;
  public valorConfiguracion: string;

  public pkidconfiguracion: string;
  public valorConfiguracionEdit: string;


  constructor(public navCtrl: NavController, public navParams: NavParams, public conf: DatabaseConfigProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfiguracionPage');
  }

  guardarConfiguracion() {
    let newConfiguracion = [{
      claveconfiguracion: this.claveConfiguracion,
      valorconfiguracion: this.valorConfiguracion
    }]

    this.conf.addConfiguracion(newConfiguracion)
      .then(res => {
        console.log('Respuesta: ', res);
      })
      .catch(error => {
        console.error('Error al guardar: ', error);
      })
  }

  guardar() {
    //SELECT * FROM tconfiguracion
    //TRUNCATE TABLE tconfiguracion

    let configs: any[]=[]

    for (let i = 0; i < 1; i++) {

      let valor="aass";
      let clave="ss22";
      let newConfiguracion = {
        claveconfiguracion: clave,
        valorconfiguracion: valor
      }
      configs.push(newConfiguracion);
      
    }

    this.conf.addConfiguracion(configs)
      .then(res => {
        console.log('Respuesta ', res);
      })
      .catch(error => {
        console.error('Error al guardar: ', error);
      })
    
  }

  actualizarConfiguracion(){

  }

}
